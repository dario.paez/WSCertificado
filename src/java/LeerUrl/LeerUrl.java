/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package LeerUrl;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.KeyStore;
import java.security.SecureRandom;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;

/**
 *
 * @author mobiltech
 */
public class LeerUrl {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        pruebaLectura();
    }

    public static void pruebaLectura() {

        boolean estado = false;

        URL url = null;
        HttpsURLConnection conn = null;
        
        
        System.setProperty("javax.net.ssl.keyStore", "/Library/Java/JavaVirtualMachines/jdk1.8.0_151.jdk/Contents/Home/jre/lib/security/cacerts");
        System.setProperty("javax.net.ssl.keyStorePassword", "changeit");
        System.setProperty("javax.net.ssl.trustStore", "/Library/Java/JavaVirtualMachines/jdk1.8.0_151.jdk/Contents/Home/jre/lib/security/cacerts");
        System.setProperty("javax.net.ssl.trustStorePassword", "changeit");

        try {

            // Obtener objeto URL
            url = new URL("https://wspre.allianzseguros.com/drswoc16/services/AutosIndividualWS?wsdl");

            conn = (HttpsURLConnection) url.openConnection();
            conn.setConnectTimeout(5000);
            conn.setReadTimeout(60000);

            // Asignar request header necesarios para la conexion
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            
            
            
               File pKeyFile = new File("/Users/mobiltech/Downloads/Allianz.pfx");
            String pKeyPassword = "allianz2017";
            KeyManagerFactory keyManagerFactory = KeyManagerFactory.getInstance("SunX509");
            KeyStore keyStore = KeyStore.getInstance("PKCS12");
            try (InputStream keyInput = new FileInputStream(pKeyFile)) {
                keyStore.load(keyInput, pKeyPassword.toCharArray());
            }
            keyManagerFactory.init(keyStore, pKeyPassword.toCharArray());
            SSLContext context = SSLContext.getInstance("TLS");
            context.init(keyManagerFactory.getKeyManagers(), null, new SecureRandom());
            SSLSocketFactory sockFact = context.getSocketFactory();
            conn.setSSLSocketFactory(sockFact);
            
            
            

            conn.setDoInput(true);
            conn.setDoOutput(true);

            if (conn.getResponseCode() != 200) {
                throw new Exception("  : Error " + conn.getResponseCode() + " : " + conn.getResponseMessage());
            }

            // =====================================================================================================================================================
            //
            // Obtener respuesta de la conexion
            BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));

            String responseJSON = "";
            String respuesta = "";
            
            
            
            
            
            
            // Leer la respuesta obtenida
            while ((respuesta = rd.readLine()) != null) {
                responseJSON += respuesta;
            }
            System.out.println("respuesta |  " + responseJSON);
            rd.close();
//
//            // Imprimir la respuesta JSON para DEBUG
//            logMsg.loggerMessageDebug("|Response|BOLIVAR|" + new JSONObject(responseJSON).toString(3));
//            // =====================================================================================================================================================
//            //
//            System.out.println("antes de El  DTO");
//            datosRespuesta = new ObjectMapper().readValue(responseJSON, RespuestaBolivarCotizacionDTO.class);
//            // Mapear la respuesta JSON obtenida
//            System.out.println("despues  de El  DTO");

        } catch (Exception e) {
            System.out.println(e);

        } finally {
        }
    }

}
