package co.mobiltech.allianz.ResponseChargeResponseDTO;


import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
    "Status",
    "Errors",
    "QuotationNumber",
    "RiskTypeDesc",
    "QuotationDate",
    "TermDate",
    "Package",
    "EffectiveDate",
    "VehicleDetails"
})
public class ChargeResponse {

    @JsonProperty("Status")
    private String status;
    @JsonProperty("Errors")
    private List<Error> errors = null;
    @JsonProperty("QuotationNumber")
    private Integer quotationNumber;
    @JsonProperty("RiskTypeDesc")
    private String riskTypeDesc;
    @JsonProperty("QuotationDate")
    private Integer quotationDate;
    @JsonProperty("TermDate")
    private Integer termDate;
    @JsonProperty("Package")
    private List<Package> _package = null;
    @JsonProperty("EffectiveDate")
    private Integer effectiveDate;
    @JsonProperty("VehicleDetails")
    private VehicleDetails vehicleDetails;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("Status")
    public String getStatus() {
        return status;
    }

    @JsonProperty("Status")
    public void setStatus(String status) {
        this.status = status;
    }

    @JsonProperty("Errors")
    public List<Error> getErrors() {
        return errors;
    }

    @JsonProperty("Errors")
    public void setErrors(List<Error> errors) {
        this.errors = errors;
    }

    @JsonProperty("QuotationNumber")
    public Integer getQuotationNumber() {
        return quotationNumber;
    }

    @JsonProperty("QuotationNumber")
    public void setQuotationNumber(Integer quotationNumber) {
        this.quotationNumber = quotationNumber;
    }

    @JsonProperty("RiskTypeDesc")
    public String getRiskTypeDesc() {
        return riskTypeDesc;
    }

    @JsonProperty("RiskTypeDesc")
    public void setRiskTypeDesc(String riskTypeDesc) {
        this.riskTypeDesc = riskTypeDesc;
    }

    @JsonProperty("QuotationDate")
    public Integer getQuotationDate() {
        return quotationDate;
    }

    @JsonProperty("QuotationDate")
    public void setQuotationDate(Integer quotationDate) {
        this.quotationDate = quotationDate;
    }

    @JsonProperty("TermDate")
    public Integer getTermDate() {
        return termDate;
    }

    @JsonProperty("TermDate")
    public void setTermDate(Integer termDate) {
        this.termDate = termDate;
    }

    @JsonProperty("Package")
    public List<Package> getPackage() {
        return _package;
    }

    @JsonProperty("Package")
    public void setPackage(List<Package> _package) {
        this._package = _package;
    }

    @JsonProperty("EffectiveDate")
    public Integer getEffectiveDate() {
        return effectiveDate;
    }

    @JsonProperty("EffectiveDate")
    public void setEffectiveDate(Integer effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    @JsonProperty("VehicleDetails")
    public VehicleDetails getVehicleDetails() {
        return vehicleDetails;
    }

    @JsonProperty("VehicleDetails")
    public void setVehicleDetails(VehicleDetails vehicleDetails) {
        this.vehicleDetails = vehicleDetails;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }
    
     public String toStringJson() {
        String dtoJsonString = null;
        try {
            ObjectMapper mapper = new ObjectMapper();
            dtoJsonString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(this);
        } catch (Exception e) {
        }
        return dtoJsonString;
    }

}
