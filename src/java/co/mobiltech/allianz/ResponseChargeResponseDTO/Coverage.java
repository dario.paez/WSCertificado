
package co.mobiltech.allianz.ResponseChargeResponseDTO;


import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import java.util.HashMap;
import java.util.Map;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
    "Deductible",
    "CoverageId",
    "CoverageName",
    "InsuredValue"
})
public class Coverage {

    @JsonProperty("Deductible")
    private Integer deductible;
    @JsonProperty("CoverageId")
    private Integer coverageId;
    @JsonProperty("CoverageName")
    private String coverageName;
    @JsonProperty("InsuredValue")
    private Double insuredValue;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("Deductible")
    public Integer getDeductible() {
        return deductible;
    }

    @JsonProperty("Deductible")
    public void setDeductible(Integer deductible) {
        this.deductible = deductible;
    }

    @JsonProperty("CoverageId")
    public Integer getCoverageId() {
        return coverageId;
    }

    @JsonProperty("CoverageId")
    public void setCoverageId(Integer coverageId) {
        this.coverageId = coverageId;
    }

    @JsonProperty("CoverageName")
    public String getCoverageName() {
        return coverageName;
    }

    @JsonProperty("CoverageName")
    public void setCoverageName(String coverageName) {
        this.coverageName = coverageName;
    }

    @JsonProperty("InsuredValue")
    public Double getInsuredValue() {
        return insuredValue;
    }

    @JsonProperty("InsuredValue")
    public void setInsuredValue(Double insuredValue) {
        this.insuredValue = insuredValue;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }
    
     public String toStringJson() {
        String dtoJsonString = null;
        try {
            ObjectMapper mapper = new ObjectMapper();
            dtoJsonString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(this);
        } catch (Exception e) {
        }
        return dtoJsonString;
    }

}
