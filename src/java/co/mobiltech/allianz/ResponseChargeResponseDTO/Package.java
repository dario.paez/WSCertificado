package co.mobiltech.allianz.ResponseChargeResponseDTO;


import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
    "Payment",
    "PackageName",
    "PackageId",
    "Coverage"
})
public class Package {

    @JsonProperty("Payment")
    private List<Payment> payment = null;
    @JsonProperty("PackageName")
    private String packageName;
    @JsonProperty("PackageId")
    private Integer packageId;
    @JsonProperty("Coverage")
    private List<Coverage> coverage = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("Payment")
    public List<Payment> getPayment() {
        return payment;
    }

    @JsonProperty("Payment")
    public void setPayment(List<Payment> payment) {
        this.payment = payment;
    }

    @JsonProperty("PackageName")
    public String getPackageName() {
        return packageName;
    }

    @JsonProperty("PackageName")
    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    @JsonProperty("PackageId")
    public Integer getPackageId() {
        return packageId;
    }

    @JsonProperty("PackageId")
    public void setPackageId(Integer packageId) {
        this.packageId = packageId;
    }

    @JsonProperty("Coverage")
    public List<Coverage> getCoverage() {
        return coverage;
    }

    @JsonProperty("Coverage")
    public void setCoverage(List<Coverage> coverage) {
        this.coverage = coverage;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }
    
     public String toStringJson() {
        String dtoJsonString = null;
        try {
            ObjectMapper mapper = new ObjectMapper();
            dtoJsonString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(this);
        } catch (Exception e) {
        }
        return dtoJsonString;
    }

}
