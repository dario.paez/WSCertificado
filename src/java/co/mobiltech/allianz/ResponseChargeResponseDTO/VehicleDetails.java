package co.mobiltech.allianz.ResponseChargeResponseDTO;


import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import java.util.HashMap;
import java.util.Map;




@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
    "Brand",
    "Type",
    "Version",
    "Class",
    "VehicleYear",
    "ProtectionDevice",
    "DiscountPNR",
    "InsuredValue",
    "AccessoriesValue",
    "GasSystemValue",
    "ShieldingValue"
})
public class VehicleDetails {

    @JsonProperty("Brand")
    private String brand;
    @JsonProperty("Type")
    private String type;
    @JsonProperty("Version")
    private String version;
    @JsonProperty("Class")
    private String _class;
    @JsonProperty("VehicleYear")
    private Integer vehicleYear;
    @JsonProperty("ProtectionDevice")
    private String protectionDevice;
    @JsonProperty("DiscountPNR")
    private Integer discountPNR;
    @JsonProperty("InsuredValue")
    private Double insuredValue;
    @JsonProperty("AccessoriesValue")
    private Integer accessoriesValue;
    @JsonProperty("GasSystemValue")
    private Integer gasSystemValue;
    @JsonProperty("ShieldingValue")
    private Integer shieldingValue;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("Brand")
    public String getBrand() {
        return brand;
    }

    @JsonProperty("Brand")
    public void setBrand(String brand) {
        this.brand = brand;
    }

    @JsonProperty("Type")
    public String getType() {
        return type;
    }

    @JsonProperty("Type")
    public void setType(String type) {
        this.type = type;
    }

    @JsonProperty("Version")
    public String getVersion() {
        return version;
    }

    @JsonProperty("Version")
    public void setVersion(String version) {
        this.version = version;
    }

    @JsonProperty("Class")
    public String getClass_() {
        return _class;
    }

    @JsonProperty("Class")
    public void setClass_(String _class) {
        this._class = _class;
    }

    @JsonProperty("VehicleYear")
    public Integer getVehicleYear() {
        return vehicleYear;
    }

    @JsonProperty("VehicleYear")
    public void setVehicleYear(Integer vehicleYear) {
        this.vehicleYear = vehicleYear;
    }

    @JsonProperty("ProtectionDevice")
    public String getProtectionDevice() {
        return protectionDevice;
    }

    @JsonProperty("ProtectionDevice")
    public void setProtectionDevice(String protectionDevice) {
        this.protectionDevice = protectionDevice;
    }

    @JsonProperty("DiscountPNR")
    public Integer getDiscountPNR() {
        return discountPNR;
    }

    @JsonProperty("DiscountPNR")
    public void setDiscountPNR(Integer discountPNR) {
        this.discountPNR = discountPNR;
    }

    @JsonProperty("InsuredValue")
    public Double getInsuredValue() {
        return insuredValue;
    }

    @JsonProperty("InsuredValue")
    public void setInsuredValue(Double insuredValue) {
        this.insuredValue = insuredValue;
    }

    @JsonProperty("AccessoriesValue")
    public Integer getAccessoriesValue() {
        return accessoriesValue;
    }

    @JsonProperty("AccessoriesValue")
    public void setAccessoriesValue(Integer accessoriesValue) {
        this.accessoriesValue = accessoriesValue;
    }

    @JsonProperty("GasSystemValue")
    public Integer getGasSystemValue() {
        return gasSystemValue;
    }

    @JsonProperty("GasSystemValue")
    public void setGasSystemValue(Integer gasSystemValue) {
        this.gasSystemValue = gasSystemValue;
    }

    @JsonProperty("ShieldingValue")
    public Integer getShieldingValue() {
        return shieldingValue;
    }

    @JsonProperty("ShieldingValue")
    public void setShieldingValue(Integer shieldingValue) {
        this.shieldingValue = shieldingValue;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }
    
     public String toStringJson() {
        String dtoJsonString = null;
        try {
            ObjectMapper mapper = new ObjectMapper();
            dtoJsonString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(this);
        } catch (Exception e) {
        }
        return dtoJsonString;
    }

}
