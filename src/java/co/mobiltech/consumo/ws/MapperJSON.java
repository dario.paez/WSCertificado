/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.mobiltech.consumo.ws;

import co.mobiltech.allianz.ResponseChargeResponseDTO.ResponseChargeResponseDTO;
import com.fasterxml.jackson.databind.ObjectMapper;

public class MapperJSON {

    public static void main(String[] args) {

        try {

            String xmlResponse = "{\"ChargeResponse\": {\n"
                    + "   \"Status\": \"B\",\n"
                    + "   \"Errors\": [\n"
                    + "      {\"Error\": \"Bloqueo: 768, 768, Valor asegurado no se encuentra entre los limites del 10%\"},\n"
                    + "      {\"Error\": \"714, 714, Verificar el riesgo, auto no tiene continuidad y el vehículo no es nuevo\"}\n"
                    + "   ],\n"
                    + "   \"QuotationNumber\": 5451756,\n"
                    + "   \"RiskTypeDesc\": \"Liviano Particular Familiar\",\n"
                    + "   \"QuotationDate\": 2022018,\n"
                    + "   \"TermDate\": 20181226,\n"
                    + "   \"EffectiveDate\": 20180126,\n"
                    + "   \"VehicleDetails\": {\n"
                    + "      \"Brand\": \"CHEVROLET\",\n"
                    + "      \"Type\": \"SPARK [2]\",\n"
                    + "      \"Version\": \"LT [M200] MT 1000CC SA\",\n"
                    + "      \"Class\": \"T\",\n"
                    + "      \"VehicleYear\": 2009,\n"
                    + "      \"ProtectionDevice\": \"Ninguno\",\n"
                    + "      \"DiscountPNR\": 20,\n"
                    + "      \"InsuredValue\": 1.8,\n"
                    + "      \"AccessoriesValue\": 0,\n"
                    + "      \"GasSystemValue\": 0,\n"
                    + "      \"ShieldingValue\": 0\n"
                    + "   }\n"
                    + "}}";
            ResponseChargeResponseDTO datosRespuesta = new ObjectMapper().readValue(xmlResponse, ResponseChargeResponseDTO.class);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

}
