/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.mobiltech.consumo.ws;

import co.mobiltech.allianz.ResponseChargeResponseDTO.ResponseChargeResponseDTO;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.security.KeyStore;
import java.security.SecureRandom;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.KeyManager;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import org.apache.http.conn.ssl.NoopHostnameVerifier;


import org.json.XML;

public class TestAllianzWS {

    public static void main(String[] args) {

        try {

            KeyStore clientStore = KeyStore.getInstance("PKCS12");
            clientStore.load(new FileInputStream(new File("/Users/mobiltech/Javacode/teca/WS_TECA_SEGUROS.pfx")), "allianz2017".toCharArray());
            KeyManagerFactory kmf = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
            kmf.init(clientStore, "allianz2017".toCharArray());
            KeyManager[] kms = kmf.getKeyManagers();

            KeyStore trustStore = KeyStore.getInstance("JKS");
            trustStore.load(new FileInputStream("/Library/Java/JavaVirtualMachines/jdk1.8.0_151.jdk/Contents/Home/jre/lib/security/cacerts"), "changeit".toCharArray());

            TrustManagerFactory tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
            tmf.init(trustStore);
            TrustManager[] tms = tmf.getTrustManagers();

            final SSLContext sslContext = SSLContext.getInstance("TLS");
            sslContext.init(kms, tms, new SecureRandom());
            SSLContext.setDefault(sslContext);

            HostnameVerifier hostnameVerifier = NoopHostnameVerifier.INSTANCE;

            HttpsURLConnection.setDefaultSSLSocketFactory(sslContext.getSocketFactory());
            HttpsURLConnection.setDefaultHostnameVerifier(hostnameVerifier);

            URL url = new URL("https://wspre.allianzseguros.com/drswoc16/services/AutosIndividualWS?wsdl");

            HttpsURLConnection con = (HttpsURLConnection) url.openConnection();
            con.setRequestMethod("GET");
            con.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64; Trident/7.0; rv:11.0) like Gecko");
            con.setRequestProperty("SOAPAction", "urn:call");
            con.setRequestProperty("content-type", "text/xml;charset=UTF-8");
            con.setConnectTimeout(10000);
            con.setReadTimeout(30000);
            con.setSSLSocketFactory(sslContext.getSocketFactory());
            con.setDoOutput(true);

            //<editor-fold defaultstate="collapsed" desc="El xml que se envia ">
// XML de pruebas
            String xmlSolicitud = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ws=\"http://ws.allianz.com\">\n"
                    + "    <soapenv:Header />\n"
                    + "    <soapenv:Body>\n"
                    + "        <ws:call>\n"
                    + "            <ws:xml>\n"
                    + "            	<![CDATA[\n"
                    + "                <chargerequest>\n"
                    + "                    <transactionnumber>1234512346001</transactionnumber>\n"
                    + "                    <authentication>\n"
                    + "                        <company>COL</company>\n"
                    + "                        <partnerid>CP300398</partnerid>\n"
                    + "                        <agentid>CA488180</agentid>\n"
                    + "                        <partnercode>CP300398</partnercode>\n"
                    + "                        <agentcode>1704337</agentcode>\n"
                    + "                    </authentication>\n"
                    + "                    <operationheaders>\n"
                    + "                        <operationtypecode>TA</operationtypecode>\n"
                    + "                        <productcode>1241</productcode>\n"
                    + "                    </operationheaders>\n"
                    + "                    <effectivedate>20180126</effectivedate>\n"
                    + "                    <TermDate>20181226</TermDate>\n"
                    + "                    <firstbill>1</firstbill>\n"
                    + "                    <successive>13</successive>\n"
                    + "                    <holderdoctype>C</holderdoctype>\n"
                    + "                    <holderdocnumber>1026592290</holderdocnumber>\n"
                    + "                    <isholderdriver>S</isholderdriver>\n"
                    + "                    <isholderowner>S</isholderowner>\n"
                    + "                    <ownerdoctype>C</ownerdoctype>\n"
                    + "                    <ownerdocnumber>1026592290</ownerdocnumber>\n"
                    + "                    <risktype>L0001</risktype>\n"
                    + "                    <vehicleplate>DBN009</vehicleplate>\n"
                    + "                    <vehicleorigin>480</vehicleorigin>\n"
                    + "                    <vehiclefasecoldacode>1601189</vehiclefasecoldacode>\n"
                    + "                    <vehicleyear>2009</vehicleyear>\n"
                    + "                    <riskdata>\n"
                    + "                        <ownerborndate>19800208</ownerborndate>\n"
                    + "                        <ownersex>F</ownersex>\n"
                    + "                        <repowered>N</repowered>\n"
                    + "                        <protectiondevicecode>4</protectiondevicecode>\n"
                    + "                        <accessoriesvalue>0.0</accessoriesvalue>\n"
                    + "                        <shieldingvalue>0.0</shieldingvalue>\n"
                    + "                        <gassystemvalue>0.0</gassystemvalue>\n"
                    + "                        <isnewvehicle>N</isnewvehicle>\n"
                    + "                        <insuredvalue>18000000.00</insuredvalue>\n"
                    + "                        <continuity>N</continuity>\n"
                    + "                        <circulationareadanecode>11001</circulationareadanecode>\n"
                    + "                    </riskdata>\n"
                    + "                </chargerequest>\n"
                    + "                ]]>\n"
                    + "            </ws:xml>\n"
                    + "        </ws:call>\n"
                    + "    </soapenv:Body>\n"
                    + "</soapenv:Envelope>";
//</editor-fold>

            // Send the request
            OutputStreamWriter wr = new OutputStreamWriter(con.getOutputStream());
            wr.write(xmlSolicitud);
            wr.flush();

            BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), "UTF-8"));
            StringBuilder sb = new StringBuilder();
            String line;

            int i = 1;

            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            br.close();
            // System.out.println(sb.toString());

            String xmlResponse = sb.toString();
            xmlResponse = xmlResponse.replaceAll("&lt;", "<");
            xmlResponse = xmlResponse.replaceAll("&gt;", ">");

             System.out.println("xmlResponse = " + xmlResponse);
             System.out.println("Inicio :: " + xmlResponse.indexOf("<ns:return>"));
             System.out.println("Final :: " + xmlResponse.indexOf("</ns:return>"));
             xmlResponse = xmlResponse.substring(xmlResponse.indexOf("<ns:return>") + 11, xmlResponse.indexOf("</ns:return>"));
             //BufferedWriter writer = new BufferedWriter(new FileWriter("/Users/mobiltech/Javacode/teca/respuestaxml.txt"));
//              writer.write(xmlResponse);
//              writer.close();
             System.out.println("xmlResponse = " + xmlResponse);
           // System.out.println(XML.toJSONObject(xmlResponse).toString(3));
           
            xmlResponse=XML.toJSONObject(xmlResponse).toString(3);
             BufferedWriter writer = new BufferedWriter(new FileWriter("/Users/mobiltech/Javacode/teca/respuestaxml.json"));
              writer.write(xmlResponse);
              writer.close();
            //String x = XML.toJSONObject(xmlResponse).toString(3);
           // System.out.println(x);
            ResponseChargeResponseDTO datosRespuesta = new ObjectMapper().readValue(xmlResponse, ResponseChargeResponseDTO.class);

            System.out.println("datosRespuesta = \n" + datosRespuesta.toStringJson());
            
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

}
